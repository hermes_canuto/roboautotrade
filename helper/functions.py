import datetime

import pandas as pd
import ta


def setround(value):
    return round(value, 8)


def average(lst):
    return sum(lst) / len(lst)


def setdecimal(number):
    a = float(number)
    return '{:.8f}'.format(a)


def getperiods(period, candle, start):
    # date_end = date_end[:19]
    start = datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
    date_generated = [{
        'start': start - datetime.timedelta(minutes=x),
        'end': start - datetime.timedelta(minutes=x + candle) - datetime.timedelta(seconds=1)
    } for x in range(0, (candle * period), candle)]
    # for i in date_generated:
    #     print('{} {}'.format(i['start'], i['end']))
    return date_generated


def getallperiods(config, data_start):
    return {
        'rsi_period': getperiods(config.RSI * 2, config.CANDLE_SIZE, data_start),
        'ema_curta_period': getperiods(config.EMA_CURTA * 2, config.CANDLE_SIZE, data_start),
        'ema_longa_period': getperiods(config.EMA_LONGA * 2, config.CANDLE_SIZE, data_start),
        'macd_period': getperiods(config.MACD_LONGA * 4, config.CANDLE_SIZE, data_start)
    }


def singleperiod(df, date_start, date_end):
    mask = ((df['Timestamp'] < date_start) & (df['Timestamp'] > date_end))
    frame = df.loc[mask]
    return frame


def singleperiod_ta(df, date_start, date_end):
    mask = ((df['Timestamp'] < date_start) & (df['Timestamp'] > date_end))
    frame = df.loc[mask]
    r = frame.iloc[-1:]
    return r


def getopenandclose(df, date_start, date_end):
    frame = singleperiod(df, date_start, date_end)
    return {
        "close_date": frame.iloc[-1]['timestamp'],
        "close": frame.iloc[-1]['value'],
        "open": frame.iloc[0]['value'],
        "open_date": frame.iloc[0]['timestamp'],
        "sub": setround(frame.iloc[-1]['value'] - frame.iloc[0]['value'])
    }


def getavg(df, date_start, date_end):
    r = singleperiod(df, date_start, date_end)
    return setround(r['value'].mean())


def sma(dt_period, df, debug=False):
    if debug:
        print("START - Debug SMA")
    mean_list = []
    for i in dt_period:
        frame = singleperiod(df, i['start'], i['end'])
        mean = setround(frame['value'].mean())
        # if debug:
        # print(frame)
        if debug:
            print(" start:{} ({}) - End:{} ({}) :: Avg({})".format(
                i['start'],
                frame.iloc[-1]['value'],
                i['end'],
                frame.iloc[0]['value'],
                mean)
            )
        mean_list.append(frame.iloc[-1]['value'])
    if debug:
        print("END - Debug SMA")
    return setround(average(mean_list))


def ema(dt_period, df, ema_value, candle, debug=False):
    k = setround(2 / (ema_value + 1))
    i = dt_period[0]
    r = getavg(df, i['start'], i['end'])
    sma_period = getperiods(ema_value, candle, "{}".format(i['end']))
    result_sma = sma(sma_period, df, debug=debug)
    result_ema = (r * k) + (result_sma * (1 - k))
    if debug:
        print("k", k)
        print("{} - {} - avg({}) ".format(i['start'], i['end'], r))
    return setround(result_ema)


def macd_ema(dt_period, df, ema_value, candle, debug=True):
    k = setround(2 / (ema_value + 1))
    result = []
    for i in dt_period:
        r = getavg(df, i['start'], i['end'])
        sma_period = getperiods(ema_value, candle, "{}".format(i['end']))
        result_sma = sma(sma_period, df, debug=False)
        result_ema = (r * k) + (result_sma * (1 - k))
        result.append(setround(result_ema))
        if debug:
            print("{} - {} - avg({}) ".format(i['start'], i['end'], r))
    return result


def media(x, signal, df):
    return setdecimal(df['MACD'][x:(x - 1) + signal].mean())


def isnan(value):
    if str(value) == "nan":
        value = " "
    return value


def macd(period, dt, config, debug=False):
    macd_fast = macd_ema(period['macd_fast_period'], dt, config.MACD_FAST, config.CANDLE_SIZE, debug=False)
    macd_slow = macd_ema(period['macd_slow_period'], dt, config.MACD_SLOW, config.CANDLE_SIZE, debug=False)
    f = pd.DataFrame(period['macd_slow_period'])
    df = pd.DataFrame({'close': f['start'],
                       'open': f['end'],
                       'slow': pd.Series(macd_slow),
                       'fast': pd.Series(macd_fast)})
    df['MACD'] = df.apply(lambda x: x['slow'] - x['fast'], axis=1)
    x = df.iloc[0:config.MACD_SIGNAL, :]
    media = x['MACD'].mean()
    r = x['MACD'][0] - media
    df['Media_signal'] = pd.Series(media)
    df['Historiogram'] = pd.Series(r)
    if debug:
        print('slow', "\t\t",
              'fast', "\t\t",
              'MACD', "\t\t",
              'Media_signal', "\t",
              'Historiogram')
        for index, row in df.iterrows():
            print(row['slow'], "\t",
                  isnan(row['fast']), "\t",
                  isnan(setdecimal(row['MACD'])), "\t",
                  isnan(setdecimal(row['Media_signal'])), "\t",
                  isnan(setdecimal(row['Historiogram'])))

    return r


def newframe(df, timerange):
    myframe = pd.DataFrame()
    for item in reversed(timerange):
        # print(item['start'], '  -----   ', item['end'])
        frame = singleperiod_ta(df, item['start'], item['end'])
        # print(frame)
        myframe = myframe.append(frame)
    return myframe


def ema_ta(df, periods, timerange, debug=False):
    df = newframe(df, timerange)
    rsi_result = ta.trend.ema_indicator(df['Close'], n=periods, fillna=True)
    df['ema'] = rsi_result
    if debug:
        print(df)
    rsi = rsi_result.iloc[-1]
    return rsi


def rsi_ta(df, periods, timerange, debug=False):
    df = newframe(df, timerange)
    rsi_result = ta.momentum.rsi(df['Close'], n=periods, fillna=True)
    df['rsi'] = rsi_result
    if debug:
        print(df)
    rsi = rsi_result.iloc[-1]
    return rsi


def macd_ta(df, timerange, fast, slow, signal, debug=False):
    df = newframe(df, timerange)
    close = df['Close']
    rsi_result = ta.trend.macd_signal(close, n_fast=fast, n_slow=slow, n_sign=signal, fillna=True)
    df['macd'] = rsi_result
    if debug:
        print(df)
    rsi = rsi_result.iloc[-1]
    return rsi


def mfi_ta(df, periods, timerange, debug=False):
    df = newframe(df, timerange)
    high = df['High']
    low = df['Low']
    close = df['Close']
    volume = df['Volume']
    rsi_result = ta.momentum.money_flow_index(high, low, close, volume, n=periods, fillna=True)
    df['mfi'] = rsi_result
    if debug:
        print(df)
    rsi = rsi_result.iloc[-1]
    return rsi
