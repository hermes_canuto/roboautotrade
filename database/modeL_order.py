import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from .config import ORDER, engine

e = engine(ORDER)
Base = declarative_base(e)


def loadSession(engine):
    metadata = Base.metadata
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


class tb_ordem(Base):
    __tablename__ = 'dados'
    __table_args__ = {'autoload': True}


def saveordem(simbol, profile_name, Close):
    print("Ordem para comprar : Salvando")
    now = "{}".format(datetime.datetime.now())
    now = now[:19]
    a = simbol.split('/')
    session = loadSession(engine(ORDER))
    i = tb_ordem(
        moeda_compra=a[0],
        moeda_base=a[1],
        origem=profile_name,
        data_msg=now,
        exc1="binance",
        id="{}".format(datetime.datetime.now()),
        buy0="{}".format(Close),
        usado="1"
    )
    session.add(i)
    session.commit()

    pass
