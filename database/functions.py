import pprint

import pandas as pd
from sqlalchemy import create_engine

from database.config import PROFILE, LOCAL
from database.model_check_profiler import saveprofile
from database.model_log import savelog


def engine(config):
    return create_engine(
        'mysql+pymysql://{}:{}@{}/{}'.format(config['user'], config['password'], config['host'], config['database']),
        echo=False)


def recordsettodataframe(conn, sql):
    sqlEngine = engine(conn)
    dbConnection = sqlEngine.connect()
    df = pd.read_sql(sql, dbConnection)
    # df.set_index('timestamp', inplace=True)
    pd.set_option('display.expand_frame_repr', False)
    dbConnection.close()
    return df


def getprofiles(debug=False):
    sqlEngine = engine(PROFILE)
    sql = "select *, control_symbol.symbol as legendsymbol " \
          "from profile  " \
          "inner join control_symbol on control_symbol.id=profile.symbol_id " \
          "where active = 1"
    with sqlEngine.connect() as con:
        rs = con.execute(sql)
        if debug:
            for row in rs:
                print(row)
        con.close()
        return rs


def dataframemain(date_e, date_s, symbol, debug=False):
    SQL = "select * from autotrade.vw_data where symbol='{}' and `timestamp` <= '{}' and `timestamp` >= '{}'" \
        .format(symbol, date_s, date_e)
    if debug:
        print(SQL)
    dt = recordsettodataframe(LOCAL, SQL)
    return dt


def save_log(entrada, saida, debug=False):
    entrada = entrada.__dict__
    entrada_dic = {}
    list_check = ['__module__',
                  '__dict__',
                  '__weakref__',
                  '__doc__']
    for item in entrada:
        if (item in list_check) == False:
            entrada_dic.update({item.lower(): entrada[item]})
    log = {
        "entrada": entrada_dic,
        "saida": saida,
    }
    # salva no banco
    savelog(log)

    saveprofile(log['entrada']['profile'])

    if debug:
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(log)
    pass
