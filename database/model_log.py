from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from .config import LOCAL, engine

e = engine(LOCAL)
Base = declarative_base(e)


def loadSession(engine):
    metadata = Base.metadata
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


class tb_technical_analysis_log(Base):
    __tablename__ = 'tb_technical_analysis_log'
    __table_args__ = {'autoload': True}


def savelog(data):
    session = loadSession(engine(LOCAL))
    i = tb_technical_analysis_log(log=data, profile=data['entrada']['profile'])
    session.add(i)
    session.commit()





