from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from .config import LOCAL, engine

e = engine(LOCAL)
Base = declarative_base(e)


def loadSession(engine):
    metadata = Base.metadata
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


class tb_check_profiler(Base):
    __tablename__ = 'tb_check_profile_order'
    __table_args__ = {'autoload': True}


def saveprofile(profile):
    session = loadSession(engine(LOCAL))
    try:
        rs = session.query(tb_check_profiler).filter(tb_check_profiler.profile == profile).limit(1)
        if rs[0].alloworder == 0:
            return True
        else:
            return False
    except:
        i = tb_check_profiler(profile=profile)
        session.add(i)
        session.commit()


def getprofilealloworder(profile):
    session = loadSession(engine(LOCAL))
    rs = session.query(tb_check_profiler).filter(tb_check_profiler.profile == profile).limit(1)
    if rs[0].alloworder == 1:
        return True
    else:
        return False


def setprofilealloworder(profile, status):
    session = loadSession(engine(LOCAL))
    rs = session.query(tb_check_profiler).filter(tb_check_profiler.profile == profile).limit(1)
    rs[0].alloworder = status
    session.commit()
