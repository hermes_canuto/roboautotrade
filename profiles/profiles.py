import datetime

now = "{}".format(datetime.datetime.now())
now = now[:19]
allsetup = [
    {
        'SYMBOL': 'ETH/BTC',
        'DATE_START': '2019-09-17 07:00:00',
        'DATE_END': '2019-09-17 15:00:00',
        'CANDLE_SIZE': 5,
        'RSI': 5,
        "RSI_ALTA": 20,
        "RSI_BAIXA": 15,
        'SMA': 3,
        "EMA": 7,
        "EMA_ALTA": 15,
        "EMA_BAIXA": 10,
        'MACD': {"FAST": 10, "SLOW": 15, "SIGNAL": 3}
    },
    {
        'SYMBOL': 'ETH/BTC',
        'DATE_START': now,
        'DATE_END': "2019-09-09 07:30:00",
        'CANDLE_SIZE': 60,
        'RSI': 3,
        "RSI_ALTA": 70,
        "RSI_BAIXA": 30,
        'SMA': 3,
        'EMA': 7,
        "EMA_ALTA": 10,
        "EMA_BAIXO": 5,
        'MACD': {"FAST": 10, "SLOW": 5}
    },
    {
        'SYMBOL': 'ETH/BTC',
        'DATE_START': "2019-08-25 07:00:00",
        'DATE_END': "2019-09-09 07:30:00",
        'CANDLE_SIZE': 5,
        'RSI': 3,
        "RSI_ALTA": 10,
        "RSI_BAIXA": 5,
        'SMA': 3,
        'EMA': 3,
        "EMA_ALTA": 35,
        "EMA_BAIXO": 7,
        'MACD': {"FAST": 10, "SLOW": 5}
    },
]


class Config:
    CANDLE_SIZE = None
    SYMBOL = None

    RSI = None
    RSI_ALTA = None
    RSI_BAIXA = None

    EMA_LONGA = None
    EMA_CURTA = None

    MACD_CURTA = None
    MACD_LONGA = None
    MACD_SIGNAL = None
    MACD_ALTA = None
    MACD_BAIXA = None
