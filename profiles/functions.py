from profiles.profiles import Config


def getprofile(recordset):
    Config.PROFILE_ID = recordset.id
    Config.PROFILE = recordset.title
    Config.CANDLE_SIZE = recordset.candlesize
    Config.SYMBOL = recordset.legendsymbol

    Config.RSI = recordset.rsi
    Config.RSI_ALTA = recordset.rsi_alta
    Config.RSI_BAIXA = recordset.rsi_baixa

    Config.EMA_LONGA = recordset.ema_longa
    Config.EMA_CURTA = recordset.ema_curta

    Config.MACD_CURTA = recordset.macd_longa
    Config.MACD_LONGA = recordset.macd_curta
    Config.MACD_SIGNAL = recordset.macd_signal
    Config.MACD_ALTA = recordset.macd_alta
    Config.MACD_BAIXA = recordset.macd_baixa

    Config.macd_period = recordset.macd_period
    Config.ema_period = recordset.ema_period
    Config.rsi_period = recordset.rsi_period

    return Config
