from helper.functions import rsi_ta, ema_ta, macd_ta, setdecimal


def rules(dt, period, DATE_START, Config, debug=False):
    data_out = {}
    data_out['ok'] = "NOK"
    ok = 0
    print("-" * 50)
    print(DATE_START, "Candle", Config.CANDLE_SIZE, Config.SYMBOL)

    if Config.RSI != 0:
        rsi = rsi_ta(dt, Config.RSI, period['rsi_period'], False)
        data_out['rsi'] = rsi
        print("RSI ({}) = {}  RSI_BAIXA({}) RSI_ALTA({})".format(Config.RSI, rsi, Config.RSI_BAIXA, Config.RSI_ALTA))
        if rsi > Config.RSI_BAIXA:
            print('RSI = Compra')
            data_out['rsi_result'] = "OK"
            ok = ok + 1
        else:
            print("RSI = NÂO FAZ NADA")
            data_out['rsi_result'] = "NOK"
    else:
        print('RSI = 0 =  Compra')
        data_out['rsi_result'] = "OK"
        ok = ok + 1

    if Config.EMA_LONGA != 0 and Config.EMA_CURTA != 0:

        ema_longa = ema_ta(dt, Config.EMA_LONGA, period['ema_longa_period'], False)
        data_out['ema_longa'] = ema_longa
        print("EMA_LONGA({}) = {}".format(Config.EMA_LONGA, ema_longa))

        ema_curta = ema_ta(dt, Config.EMA_CURTA, period['ema_curta_period'], False)
        data_out['ema_curta'] = ema_curta
        print("EMA_CURTA({}) = {}".format(Config.EMA_CURTA, ema_curta))

        if ema_longa < ema_curta:
            print('EMA = Compra')
            data_out['ema_result'] = "OK"
            ok = ok + 1
        else:
            print("EMA = NÂO FAZ NADA")
            data_out['ema_result'] = "NOK"
    else:
        print('EMA = 0 = Compra')
        data_out['ema_result'] = "OK"
        ok = ok + 1

    if Config.MACD_CURTA != 0 and  Config.MACD_LONGA != 0 and Config.MACD_SIGNAL != 0:
        macd = macd_ta(dt, period['macd_period'], Config.MACD_CURTA, Config.MACD_LONGA, Config.MACD_SIGNAL, False)
        data_out['macd'] = macd
        print("MACD({}) LONGA({}) CURTA({}) SIGNAL ({}) ALTA({}) BAIXA ({})".format(setdecimal(macd),
                                                                                    Config.MACD_CURTA,
                                                                                    Config.MACD_LONGA,
                                                                                    Config.MACD_SIGNAL,
                                                                                    Config.MACD_ALTA,
                                                                                    Config.MACD_BAIXA))
        if macd > Config.MACD_ALTA:
            print('MACD Compra')
            data_out['macd_result'] = "OK"
            ok = ok + 1
        else:
            print("MACD = NÂO FAZ NADA")
            data_out['macd_result'] = "NOK"
    else:
        print('MACD =0 =  Compra')
        data_out['macd_result'] = "OK"
        ok = ok + 1

    if ok == 3:
        data_out['ok'] = "OK"

    print('-' * 90)
    return data_out
