import datetime
import sys
import warnings

import pandas as pd

from database.config import LOCAL
from database.functions import dataframemain, getprofiles, recordsettodataframe, save_log
from database.modeL_order import saveordem
from database.model_check_profiler import getprofilealloworder, setprofilealloworder
from helper.functions import getallperiods
from profiles.functions import getprofile
from profiles.profiles import allsetup
from rules import rules

warnings.simplefilter("ignore")

try:
    for item in sys.argv:
        profile_id = str(item)

    profile_id = int(profile_id)

    if len(allsetup) < profile_id:
        print("Setup Not Found")
        print("Setup default")
        profile_id = 0
except:
    profile_id = 0

dt_format = '%Y-%m-%d %H:%M:%S.%f'

rs_profiles = getprofiles()

# start_date = datetime.datetime.now() - datetime.timedelta(hours=3)
start_date = datetime.datetime.now()
end_date = start_date - datetime.timedelta(days=1)
now = "{}".format(start_date)
end = "{}".format(end_date)

DATE_START = now[:19]
DATE_END = end[:19]

for rs in rs_profiles:
    print(rs.title)
    Config = getprofile(rs)
    Config.data_start = DATE_START
    Config.data_end = DATE_END

    # define todos os ranges dos candles data-inicio / data-fim
    period = getallperiods(Config, DATE_START)

    # carrega os dados do banco
    dt = dataframemain(DATE_END, DATE_START, Config.SYMBOL)
    close = dt.iloc[0]['Close']
    # Executa as regras e retorna as respostas
    data_out = rules(dt, period, DATE_START, Config)

    # busca o padrão para relação com ordem
    w = sorted([Config.macd_period, Config.ema_period, Config.rsi_period], reverse=True)
    print(w)
    sql = "select ema,macd,rsi from .vw_log_ok where profile='{}' order by id desc limit {}".format(Config.PROFILE,
                                                                                                    w[0])
    dt = recordsettodataframe(LOCAL, sql)
    print("Padrão origem do banco de dados")
    print(dt)

    # salva o log
    save_log(Config, data_out)

    # Verifica ordem
    # data_out['ok'] = 'ok'

    if data_out['ok'] == "ok":
        # Configura o padrão para comparação , para poder fazer a ordem
        df = pd.DataFrame()
        df['ema'] = pd.Series(["OK" for i in range(Config.ema_period)])
        df['macd'] = pd.Series(["OK" for i in range(Config.macd_period)])
        df['rsi'] = pd.Series(["OK" for i in range(Config.rsi_period)])
        df.apply(lambda col: col.drop_duplicates().reset_index(drop=True))
        df.fillna('NOK', inplace=True)
        print("Padrão-de-Periodos-do-Perfil")
        print(df)

        if df.equals(dt):
            print("Padrão Positivo")
            if getprofilealloworder(Config.PROFILE):
                print("Está liberado a compra")
                setprofilealloworder(Config.PROFILE, False)
                profile = "at-{}-{}".format(Config.PROFILE, Config.PROFILE_ID)
                saveordem(Config.SYMBOL, Config.PROFILE, close)
            else:
                print("Pode comprar mas não liberado")
    else:
        print("Ordem Negada : Formulas:Negativo")
        print("Liberado para proximo positivo")
        setprofilealloworder(Config.PROFILE, True)

    print("\n")
    print("*" * 50)
